# Fedora Docs pages

Pages such as the homepage for Fedora Docs. Previously hosted in the [docs-fp-o](https://pagure.io/fedora-docs/docs-fp-o/) repository under `/pages/`.
